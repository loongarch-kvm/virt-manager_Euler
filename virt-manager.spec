%global __python %{__python3}

Name:                virt-manager
Version:             4.1.0
Release:             2
Summary:             The manage virtual machines tool which via libvirt.
License:             GPLv2+
BuildArch:           noarch
URL:                 https://virt-manager.org/
Source0:             https://virt-manager.org/download/sources/virt-manager/virt-manager-%{version}.tar.gz

Patch1:              Add-loongarch-support.patch

Requires:            virt-manager-common = %{version}-%{release} python3-gobject gtk3 libvirt-glib >= 0.0.9
Requires:            gtk-vnc2 dconf vte291 gtksourceview4
Recommends:          (libvirt-daemon-kvm or libvirt-daemon-qemu) libvirt-daemon-config-network
BuildRequires:       git gettext python3-devel python3-docutils
Suggests:            python3-libguestfs


%description
The virtual machine management tool uses libvirt as the management API and provides
graphical tools for managing KVM,Xen and LXC.Used to start, stop, add or delete
virtual devices, Connect to the console via graphics or serial to view and count the
resource usage and provide it to the virtual machine.

%package             common
Summary:             Files used for Virtual Machine Manager interfaces
Requires:            python3-argcomplete python3-libvirt python3-libxml2 python3-requests
Requires:            libosinfo >= 0.2.10 python3-gobject-base xorriso

%description         common
The files used by virt-manager interfaces, as virt-install related tools.

%package -n virt-install
Summary:             Utilities for installing virtual machines
Requires:            virt-manager-common = %{version}-%{release} libvirt-client
Provides:            virt-install virt-clone virt-xml

%description -n virt-install
Package provides several command line utilities, including virt-clone (clone an
existing virtual machine) and virt-install (build and install new VMs).

%package             help
Summary:             Documentation for user of virt-manager.

%description         help
Documentation for user of virt-manager.

%prep
%autosetup -n virt-manager-%{version} -p1

%build
./setup.py configure --default-hvs "qemu,xen,lxc" --default-graphics=vnc

%install
./setup.py --no-update-icon-cache --no-compile-schemas install -O1 --root=%{buildroot}
%find_lang virt-manager
for f in $(find %{buildroot} -type f -executable -print); do
    sed -i "1 s|^#!/usr/bin/env python3|#!%{__python3}|" $f || :
done

%if 0%{?py_byte_compile:1}
%py_byte_compile %{__python3} %{buildroot}%{_datadir}/virt-manager/
%endif

%files
%doc README.md COPYING NEWS.md
%{_bindir}/virt-manager
%{_datadir}/virt-manager/ui/*.ui
%{_datadir}/virt-manager/{virt-manager,virtManager,icons}
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/applications/virt-manager.desktop
%{_datadir}/glib-2.0/schemas/org.virt-manager.virt-manager.gschema.xml
%{_datadir}/metainfo/virt-manager.appdata.xml

%files common -f virt-manager.lang
%dir %{_datadir}/virt-manager
%{_datadir}/virt-manager/{virtcli,virtconv,virtinst}

%files -n virt-install
%{_datadir}/bash-completion/completions/{virt-install,virt-clone,virt-convert,virt-xml}
%{_bindir}/{virt-install,virt-clone,virt-xml}

%files               help
%{_mandir}/man1/virt-manager.1*
%{_mandir}/man1/{virt-install.1*,virt-clone.1*,virt-xml.1*}

%changelog
* Tue Jan 10 2023 zhaotianrui <zhaotianrui@loongson.cn> - 4.1.0-2
- Add loongarch support

* Thu Sep 08 2022 wangdi <wangdi@kylinos.cn> - 4.1.0-1
- Upgrade to 4.1.0

* Mon May 30 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 3.2.0-1
- Upgrade to 3.2.0

* Thu Feb 17 2022 liuxingxiang <liuxingxiang@kylinsec.com.cn> - 2.1.0-6
- Improve simplified Chinese translation

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.1.0-5
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 2.1.0-4
- Completing build dependencies to fix git command missing error

* Wed Apr 22 2020 Jeffery.Gao <gaojianxing@huawei.com> - 2.1.0-3
- Package init
